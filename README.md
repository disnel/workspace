# Proyecto de ejemplo **Workspace**

## Requerimientos
React, Javascript, StyledComponents o CSS, HTML5. Subir la solución a un repositorio en
Github/GitLab.

## Descripción
Crear una vista para configurar un espacio de trabajo en la web. Consiste en un
formulario con elementos visuales para recopilar la información, que a la derecha tendrá
un componente visual para mostrar parte de esas configuraciones.

## Responsive
Desktop: 2 columnas o vistas laterales
Móviles: 1 columna (solo el formulario)

## Dudas y preguntas
1. ¿Dónde encuentro la parte derecha del diseño? ¿O sea la columna que muestra parte de las configuraciones?
2. Hay estilos en el diseño que no tienen las especificaciones (p.ej. el sombreado de los radios). ¿Dónde puedo encontrarlos?
3. Al instalar los paquetes con `yarn add` salieron varios avisos de dependencias incumplidas. Busqué un poco y creo que tiene que ver con la versión de `yarn` (tengo la 1.19.1). Otros aconsejan utilizar `npm`, pero en otro proyecto de ejemplo no me dejó instalar nada. ¿Les ha ocurrido? ¿Alguna sugerencia?
4. En el caso del componente `Upload`, no sé como especificar el `action` para subir el archivo. No sé si es necesario contar con un REST API que reciba ese fichero como argumento, la documentación no es muy precisa al respecto.
