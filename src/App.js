import './App.scss';
import WorkspaceForm from "./components/WorkspaceForm";
import { Row, Col, Image } from 'antd';
import Details from './details.svg';

function App() {
  return (
    <div className="App">
      <Row className="app-inner-wrapper" justify="space-between" align="middle">
        <Col xs={24} lg={12}>
          <WorkspaceForm />
        </Col>
        <Col xs={0} lg={10}>
          <Image className="floating-preview" src={Details} preview={false} />
        </Col>
      </Row>
    </div>
  );
}

export default App;
