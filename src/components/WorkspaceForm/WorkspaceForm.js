/*
 * External dependencies
 */
import 'antd/dist/antd.css';
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { useState } from 'react';
import { Form, Button, Space } from 'antd';
import FormBuilder from 'antd-form-builder';

/*
 * Internal dependencies
 */
import ColorPicker from '../ColorPicker';
import LogoUpload from '../LogoUpload';
import HelpText from '../HelpText';
import SquareRadioGroup from '../SquareRadioGroup';

function WorkspaceForm() {
  const [color, setColor] = useState("#39b0ff");

  const [form] = Form.useForm();
  const forceUpdate = FormBuilder.useForceUpdate();
  const meta = {
    formItemLayout: [24, 24],
    fields: [
      {
        key: 'logo',
        label: 'Logo del espacio',
        widget: LogoUpload,
        help: <HelpText texts={
          [
            'Este logo identificará tu espacio entre el resto.',
            'Preferiblemente sube una imagen .png igual o superior a 65px a 72ppp con fondo transparente.'
          ]
        } />
      },
      {
        key: 'name',
        label: 'Nombre del espacio',
        widgetProps: { placeholder: 'Ep: Mi espacio de trabajo', className: `color-${color.substring(1)}` }
      },
      {
        key: 'url',
        label: 'URL del espacio (dirección web)',
        widgetProps: { placeholder: 'Ep: mi.dominio', suffix: '.dofleini.com', className: `color-${color.substring(1)}` },
        help: <HelpText icon={<ExclamationCircleOutlined />} texts={[
          'Puedes cambiar la URL de tu espacio (dirección web) en cualquier momento, pero por cortesía hacia tus compañeros de trabajo y otros usuarios de Plankton, porfavor no lo hagas muy seguido :)',
          'Nota: Si cambias la URL de tu espacio, Plankton automáticamente redireccionará desde la antigua dirección hacia la nueva. En cualquier caso, deberías asegurarte que tus compañeros sepan acerca del cambio porque la dirección anterior pasará a estar libre y puede ser usada por otro espacio en el futuro.'
        ]} />
      },
      {
        key: 'space_size',
        label: '¿Cuántas personas trabajarán contigo, incluyéndote a ti?',
        widget: SquareRadioGroup,
        widgetProps: {
          color: color,
          width: 'auto',
          padding: 'small',
          options: [
            { value: 'me', text: 'Sólo yo' },
            { value: '2-10', text: '2 - 10' },
            { value: '11-25', text: '11 - 25' },
            { value: '26-50', text: '26 - 50' },
            { value: '51-100', text: '51 - 100' },
            { value: '+500', text: '500 +' },
          ]
        }
      },
      {
        key: 'color_picker',
        label: 'Color del tema',
        widget: ColorPicker,
        widgetProps: { color: color, setColor: setColor }
      },
      {
        key: 'space_priv',
        label: 'Privacidad del espacio',
        widget: SquareRadioGroup,
        widgetProps: {
          color: color,
          width: 300,
          padding: 'big',
          options: [
            { value: 'priv', text: 'Privado' },
            { value: 'pub', text: 'Público' }
          ]
        }
      }
    ],
  }

  return (
    <>
      <h1 className="title">Configuración</h1>
      <Form form={form} onValuesChange={forceUpdate}>
        <FormBuilder meta={meta} form={form} />
        <Form.Item wrapperCol={{ span: 24, offset: 0 }}>
          <Space>
            <Button type="primary">Guardar cambios</Button>
            <Button type="secondary">Descartar</Button>
          </Space>
        </Form.Item>
      </Form>
    </>
  )
}

export default WorkspaceForm;
