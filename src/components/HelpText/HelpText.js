import { Typography, Row, Col } from 'antd';

function HelpText(props) {
  const { Paragraph } = Typography;

  return (
    <>
      <Row className="help-text">
        {props.icon ? (
          <Col span={1}>
            {props.icon}
          </Col>
        ) : (
            null
          )}
        <Col span={props.icon ? 23 : 24}>
          {
            props.texts.map((item, index) => {
              return (
                <Paragraph key={index}>{item}</Paragraph>
              )
            })
          }
        </Col>
      </Row>
    </>
  )
}

export default HelpText;
