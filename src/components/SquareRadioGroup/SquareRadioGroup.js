import { Radio } from 'antd';

function SquareRadioGroup(props) {
  const color = props.color.substring(1);
  const Options = props.options.map(option => {
    return (
      <Radio
        key={option.value}
        value={option.value}
        className={`p-${props.padding}`}
      >
        {option.text}
      </Radio>
    )
  })
  return (
    <Radio.Group
      name="sqradio"
      className={`color-${color} w-${props.width}`}
    >
      {Options}
    </Radio.Group>
  )
}

export default SquareRadioGroup;
