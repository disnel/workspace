import { Button, Image, Row, Col, Upload, message } from 'antd';
import { useState } from 'react'; 
import { UploadOutlined } from '@ant-design/icons';
import AvatarPlaceholder from './avatar.png';

function LogoUpload(props) {
  const [file, setFile] = useState('');

  const avatar_props = {
    name: 'file',
    action: 'http://localhost/sites/test/upload.php',
    showUploadList: false,
    maxCount: 1,
    onChange(info) {
      if (info.file.status !== 'uploading') {
        //console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
        setFile(info.file.name);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };
  return (
    <>
      <Row align="middle">
        <Col span={4} className="avatar-img">
          <Image src={file ? `http://localhost/sites/test/uploads/${file}` : AvatarPlaceholder} width={67} preview={false} />
        </Col>
        <Col span={10}>
          <Upload {...avatar_props}>
            <Button icon={<UploadOutlined />}>Subir logo</Button>
          </Upload>
        </Col>
      </Row>
    </>
  )
}

export default LogoUpload;
