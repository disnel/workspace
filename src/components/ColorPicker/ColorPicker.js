import { CirclePicker } from 'react-color';

function ColorPicker(props) {
  const colors = ["#39B0FF", "#04B58B", "#3E9C4B", "#B6BC00", "#E59100", "#E55C00", "#EE1F50", "#D6198A", "#B321F1"]
  return (
    <CirclePicker
      width="600px"
      color={props.color}
      colors={colors}
      onChange={(color, event) => props.setColor(color.hex)}
      circleSize={45}
      circleSpacing={18}
    />
  )
}

export default ColorPicker;
